CREATE DATABASE Notes
GO

USE Notes
Go

CREATE TABLE [dbo].[TbNote] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [note] varchar(255) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  [user_id] int  NOT NULL,
  [register_date] datetime  NOT NULL,
  [update_date] datetime  NULL,
  [is_delete] bit  NOT NULL,
  CONSTRAINT [PK__note__3213E83FE081110F] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]
GO
ALTER TABLE [dbo].[TbNote] SET (LOCK_ESCALATION = TABLE)
GO

CREATE TABLE [dbo].[TbUser] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] varchar(200) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  [last_name] varchar(200) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  [email] varchar(100) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  [password] varchar(100) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  [rol] varchar(10) COLLATE Modern_Spanish_CI_AS  NOT NULL,
  CONSTRAINT [PK__user__3213E83FA635B5F0] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]
GO
ALTER TABLE [dbo].[TbUser] SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE [dbo].[TbNote] ADD CONSTRAINT [fk_note_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[TbUser] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- insert user

INSERT INTO [dbo].[TbUser] ([name], [last_name], [email], [password],  [rol]) VALUES ('Jose', 'Perez', 'mperez@gmail.com', '123456','USUARIO')
GO
